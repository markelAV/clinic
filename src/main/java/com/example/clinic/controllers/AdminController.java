package com.example.clinic.controllers;

import com.example.clinic.entities.*;
import com.example.clinic.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private TypeServiceRepository typeServiceRepository;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AppointmentRepository appointmentRepository;

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        return new ResponseEntity<>("Kek", HttpStatus.OK);
    }


    @PostMapping("/type-service")
    public ResponseEntity<TypeService> createTypeService(@RequestBody TypeService typeService) {
        return new ResponseEntity<>(typeServiceRepository.save(typeService), HttpStatus.CREATED);
    }

    @PutMapping("/type-service")
    public ResponseEntity<TypeService> updateTypeService(@RequestBody TypeService typeService) {
        return new ResponseEntity<>(typeServiceRepository.save(typeService), HttpStatus.OK);
    }

    @DeleteMapping("/type-service")
    public ResponseEntity<?> deleteTypeService(@RequestBody TypeService typeService) {
        typeServiceRepository.delete(typeService);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.CREATED);
    }

    @PutMapping("/employee")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
        return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
    }

    @DeleteMapping("/employee")
    public ResponseEntity<?> deleteEmployee(@RequestBody Employee employee) {
        employeeRepository.delete(employee);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/client")
    public ResponseEntity<Client> findClient(@RequestParam(name = "employee_id") Long clientId) {
        return clientRepository.findById(clientId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @PostMapping("/client")
    public ResponseEntity<Client> createEmployee(@RequestBody Client client) {
        return new ResponseEntity<>(clientRepository.save(client), HttpStatus.CREATED);
    }

    @PutMapping("/client")
    public ResponseEntity<Client> updateEmployee(@RequestBody Client client) {
        return new ResponseEntity<>(clientRepository.save(client), HttpStatus.OK);
    }

    @DeleteMapping("/client")
    public ResponseEntity<?> deleteEmployee(@RequestBody Client client) {
        clientRepository.delete(client);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/service")
    public ResponseEntity<Service> createService(@RequestBody Service service) {
        return new ResponseEntity<>(serviceRepository.save(service), HttpStatus.CREATED);
    }

    @PutMapping("/service")
    public ResponseEntity<Service> updateService(@RequestBody Service service) {
        return new ResponseEntity<>(serviceRepository.save(service), HttpStatus.OK);
    }

    @DeleteMapping("/service")
    public ResponseEntity<?> deleteService(@RequestBody Service service) {
        serviceRepository.delete(service);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/appointment")
    public ResponseEntity<Appointment> updateAppointment(@RequestBody Appointment appointment) {
        return new ResponseEntity<>(appointmentRepository.save(appointment), HttpStatus.OK);
    }

    @DeleteMapping("/appointment")
    public ResponseEntity<?> deleteAppointment(@RequestBody Appointment appointment) {
        appointmentRepository.delete(appointment);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
