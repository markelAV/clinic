package com.example.clinic.controllers;


import com.example.clinic.dto.AuthenticationRequest;
import com.example.clinic.dto.RequestRegistrationUser;
import com.example.clinic.entities.Client;
import com.example.clinic.entities.User;
import com.example.clinic.security.JwtTokenProvider;
import com.example.clinic.services.UserService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/auth")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorizeController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    /*public AuthorizeController(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }*/

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest request) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));
            User user = userService.findUserByUsername(request.getLogin()).orElseThrow();
            String token = jwtTokenProvider.createToken(request.getLogin(), user.getRole().name());
            Client client = userService.findClientByPhone(request.getLogin());
            Map<String, Object> response = new HashMap<>();
            response.put("role", user.getRole().name());
            response.put("token", token);
            response.put("typeToken", JwtTokenProvider.TYPE_TOKEN);
            if (client != null) {
                response.put("client", client);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (NoSuchElementException | AuthenticationException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>("Authentication failed. Username or password is not valid", HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) {
        //Fixme
        try {
            String token = jwtTokenProvider.resolveToken(request);
            if (token != null) {
                String userName = jwtTokenProvider.getUsername(token);
                User user = userService.findUserByUsername(userName).orElseThrow();
                return new ResponseEntity<>(HttpStatus.OK);
            }
            SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
            securityContextLogoutHandler.logout(request, response, null);
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(path = "/registration", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> registration(@RequestBody RequestRegistrationUser request) {
        Map<String, String> response = new HashMap<>();
        if (isValidCreateUserRequest(request)) {
            try {
                request.setPassword(passwordEncoder.encode(request.getPassword()));
                String username = userService.registrationUser(request);
                response.put("status", "success");
                response.put("username", username);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                response.put("status", "error");
                response.put("errorMessage", ex.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            response.put("status", "error");
            response.put("errorMessage", "User data invalid!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private boolean isValidCreateUserRequest(RequestRegistrationUser request) {
        return request != null && request.getPhone() != null;
    }


}
