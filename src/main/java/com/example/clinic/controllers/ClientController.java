package com.example.clinic.controllers;

import com.example.clinic.entities.*;
import com.example.clinic.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AppointmentRepository appointmentRepository;

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        return new ResponseEntity<>("Kek", HttpStatus.OK);
    }


    @GetMapping("/clients")
    public ResponseEntity<List<Client>> findAllClients() {
        return new ResponseEntity<>(clientRepository.findAll(), HttpStatus.OK);

    }

    @GetMapping("/client")
    public ResponseEntity<Client> findClient(@RequestParam(name = "employee_id") Long clientId) {
        return clientRepository.findById(clientId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/appointments")
    public ResponseEntity<List<Appointment>> findAllAppointments(@RequestParam(name = "service_id", required = false) Long serviceId,
                                                                 @RequestParam(name = "client_id", required = false) Long clientId) {
        if (serviceId != null) {
            Service service = new Service();
            service.setId(serviceId);
            return new ResponseEntity<>(appointmentRepository.findByService(service), HttpStatus.OK);
        }
        if (clientId != null) {
            Client client = new Client();
            client.setId(clientId);
            return new ResponseEntity<>(appointmentRepository.findByClient(client), HttpStatus.OK);
        }
        return new ResponseEntity<>(appointmentRepository.findAll(), HttpStatus.OK);

    }

    @GetMapping("/appointment")
    public ResponseEntity<Appointment> findAppointment(@RequestParam(name = "appointment_id") Long appointmentId) {
        return appointmentRepository.findById(appointmentId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @PostMapping("/appointment")
    public ResponseEntity<Appointment> createAppointment(@RequestBody Appointment appointment) {
        return new ResponseEntity<>(appointmentRepository.save(appointment), HttpStatus.CREATED);
    }
}
