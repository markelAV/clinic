package com.example.clinic.controllers;

import com.example.clinic.entities.*;
import com.example.clinic.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/main")
public class MainController {

    @Autowired
    private TypeServiceRepository typeServiceRepository;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ClientRepository clientRepository;

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        return new ResponseEntity<>("Kek", HttpStatus.OK);
    }


    @GetMapping("/type-services")
    public ResponseEntity<List<TypeService>> findAllTypeService(@RequestParam(name = "type_id", required = false) Long typeId) {
        return new ResponseEntity<>(typeServiceRepository.findAll(), HttpStatus.OK);

    }

    @GetMapping("/type-service")
    public ResponseEntity<TypeService> findService(@RequestParam(name = "type_id") Long typeId) {
        return typeServiceRepository.findById(typeId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> findAllEmployees() {
        return new ResponseEntity<>(employeeRepository.findAll(), HttpStatus.OK);

    }

    @GetMapping("/employee")
    public ResponseEntity<Employee> findEmployee(@RequestParam(name = "employee_id") Long employeeId) {
        return employeeRepository.findById(employeeId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/clients")
    public ResponseEntity<List<Client>> findAllClients() {
        return new ResponseEntity<>(clientRepository.findAll(), HttpStatus.OK);

    }

    @GetMapping("/client")
    public ResponseEntity<Client> findClient(@RequestParam(name = "employee_id") Long clientId) {
        return clientRepository.findById(clientId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @GetMapping("/services")
    public ResponseEntity<List<Service>> findAllServices(@RequestParam(name = "type_id", required = false) Long typeId) {
        if (typeId != null) {
            TypeService typeService = new TypeService();
            typeService.setId(typeId);
            return new ResponseEntity<>(serviceRepository.findByTypeService(typeService), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(serviceRepository.findAll(), HttpStatus.OK);
        }

    }

    @GetMapping("/service")
    public ResponseEntity<Service> findTypeService(@RequestParam(name = "service_id") Long serviceId) {
        return serviceRepository.findById(serviceId).map(typeService -> new ResponseEntity<>(typeService, HttpStatus.OK)).orElse(
                new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }
}
