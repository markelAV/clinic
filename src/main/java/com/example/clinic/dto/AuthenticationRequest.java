package com.example.clinic.dto;

import lombok.Data;

@Data
public class AuthenticationRequest {
    private String login;
    private String password;
}
