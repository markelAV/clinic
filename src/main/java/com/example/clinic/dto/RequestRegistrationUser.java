package com.example.clinic.dto;

import lombok.Data;

@Data
public class RequestRegistrationUser {
    private String fio;
    private String address;
    private String phone;
    private String password;
}
