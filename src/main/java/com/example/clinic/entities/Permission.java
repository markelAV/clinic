package com.example.clinic.entities;

public enum Permission {
    CLIENT("CLIENT"),
    ADMIN("ADMIN");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
