package com.example.clinic.entities;

public enum UserStatus {
    ACTIVE, BANNED
}
