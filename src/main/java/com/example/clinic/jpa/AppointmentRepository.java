package com.example.clinic.jpa;

import com.example.clinic.entities.Appointment;
import com.example.clinic.entities.Client;
import com.example.clinic.entities.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    List<Appointment> findByClient(Client client);

    List<Appointment> findByService(Service service);
}
