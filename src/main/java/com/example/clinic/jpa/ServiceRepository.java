package com.example.clinic.jpa;

import com.example.clinic.entities.Service;
import com.example.clinic.entities.TypeService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {
    List<Service> findByTypeService(TypeService typeService);
}
