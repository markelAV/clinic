package com.example.clinic.security;


import com.example.clinic.entities.User;
import com.example.clinic.entities.UserStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

@Data
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserSecurity implements UserDetails {

    String username;
    String password;
    Set<SimpleGrantedAuthority> authorities;
    boolean isActive;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isActive;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isActive;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }

    public static UserDetails convertToUserSecurity(User user) {
        return new UserSecurity(
                user.getUsername(),
                user.getPassword(),
                user.getRole().getAuthorities(),
                user.getStatus().equals(UserStatus.ACTIVE)
        );
    }
}
