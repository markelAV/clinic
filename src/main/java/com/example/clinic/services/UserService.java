package com.example.clinic.services;

import com.example.clinic.dto.RequestRegistrationUser;
import com.example.clinic.entities.Client;
import com.example.clinic.entities.User;
import com.example.clinic.entities.UserRole;
import com.example.clinic.entities.UserStatus;
import com.example.clinic.jpa.ClientRepository;
import com.example.clinic.jpa.UserRepository;
import com.example.clinic.security.UserSecurity;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return UserSecurity.convertToUserSecurity(user);
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    public Client findUserProfile(Long userId) {
        return clientRepository.findById(userId).orElseThrow();
    }

    public Client findClientByPhone(String phone) {
        return clientRepository.findByPhone(phone).orElse(null);
    }

    public User getUserByUsername(String userName) {
        return userRepository.getByUsername(userName);
    }

    public Optional<User> findUserByUsername(String phone) {
        return userRepository.findByUsername(phone);
    }


    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.getByUsername(user.getUsername());

        if (userFromDB != null) {
            return false;
        }

        //user.setRole(user.getRole());
        user.setPassword(user.getPassword());
        userRepository.save(user);
        return true;
    }

    @Transactional
    public String registrationUser(RequestRegistrationUser createUser) throws Exception {
        Client findUserByPhone = clientRepository.findByPhone(createUser.getPhone()).orElse(null);
        User user = new User();
        if (findUserByPhone == null) {
            //Todo 1) Создание Клиента и потом пользователя
            Client client = new Client();
            client.setFio(createUser.getFio());
            client.setAddress(createUser.getAddress());
            client.setPhone(createUser.getPhone());

            client = clientRepository.save(client);

            if (client.getId() != null) {
                user.setUsername(client.getPhone());
                user.setPassword(createUser.getPassword());
                user.setRole(UserRole.CLIENT);
                user.setStatus(UserStatus.ACTIVE);

                userRepository.save(user);
            } else {
                throw new Exception();
            }
        } else {
            //Fixme
            throw new Exception();
        }
        return user.getUsername();

    }

    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

}
